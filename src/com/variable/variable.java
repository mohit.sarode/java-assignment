package com.variable;

public class variable {
	
	
	int a=10;    //instance Variable
	
	
	static int b=20;  //Static Variable
	
	public void  var() {
		
		
		int c=30; //Local Variable
		
		
	}
	
	public static void main(String[] args) {
		
		variable v = new variable();
		variable v1 = new variable(); //v and v1 seprate memory allocation
		
		 a = a+20; // we cannot use instance (non-static) variable without creating object	
		System.out.println(a);
		
		b=b+10; // we can access static variable  without using an object 
		
		c=c*2;  //The Scope of local variable is only in the method
		
	}
	

}
