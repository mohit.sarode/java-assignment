package inheritance_Hierarchical;

public class Child2 extends Parent{
	public void property3() {
		System.out.println("Property 3");
	}
	
	public static void main(String[] args) {
		Parent p = new Parent();
		p.property1();    //Parent property
		//p.property2();  //Child1 property
		//p.property3();  //Child2 property
		
		Child1 c1 = new Child1();
		c1.property1();    //Parent property
		c1.property2();    //Child1 property
		//c1.property3();  //Child2 property
		
		
		Child2 c2 = new Child2();
		c2.property1();  //Parent property
		c2.property3();  //Child1 property
	//	c2.property2();  ////Child2 sproperty
	}

}
