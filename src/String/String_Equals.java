package String;

public class String_Equals {
	
	public static void main(String[] args) {
		 String s1="Welcome";
		 String s2="Welcome";
		 String s3=new String("Welcome");
		 String s4="Java";
		 
		 boolean a=s1.equals(s2);  //true
		 boolean b=s1.equals(s3);  //true
		 boolean c=s1.equals(s4);  //false
		 System.out.println(a);  
		 System.out.println(b);  
		 System.out.println(c); 
		 
		 boolean a1= s1==s2;  //true
		 boolean b1= s1==s3;  //false(it false bcz it store at different memory location(s1 is store in constant pool area and s3 is store in heap area) )
		 boolean c1= s1==s4;  //false
	}

}
