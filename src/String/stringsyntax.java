package String;

public class stringsyntax {
	
	public static void main(String[] rags) {
		
		//using string Literal
		String s1 = "Welcome"; // Strored in string constant pool area
		System.out.println("S1 String is:-"+s1);
		
		String s2 = "Welcome";  // Strored in string constant pool area
		System.out.println("S2 String is:-"+s2);
		
		//character array
		char[] ch={'s','e','l','e','n','i','u','m'};
		//converting character array into string
		String s5=new String(ch);// passing parameter as (ch)
		System.out.println(ch);
		
		//using new Keyword
		
		String s3=new String("Welcome");   // Strored in string heap area
		System.out.println("S3 String is:-"+s3);
		
		
		String s4=new String("Mohit");
		System.out.println("S4 String is:-"+s4);
		
		
		
		
		
	}

}
