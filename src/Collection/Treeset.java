package Collection;

import java.util.TreeSet;

public class Treeset {
	
	public static void main(String[] args) {
		
		//  TreeSet<String>  al=new TreeSet<String>();  
		TreeSet set = new TreeSet();
		
		set.add("A");
		set.add("B");
		set.add("c");
		set.add("c");
		set.add("D");
		set.add("E");
		set.add("F");
		
		
		System.out.println(set);
		
		 //poolFirst
		 Object p = set.pollFirst();
		 System.out.println(p);
		 
		 //poolLast
		 Object q = set.pollLast();
		 System.out.println(q);
		 
		 System.out.println("Intial Set:"+set);
		 
		 //headSet
		 System.out.println("Head Set: " +set.headSet("C"));
		 
		 //tailSet
		 System.out.println("TailSet: "+set.tailSet("D"));
		 
		 //subSet
		 
		 System.out.println("SubSet: " +set.subSet("A", "D"));
		 
		
		
	}

}
