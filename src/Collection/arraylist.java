package Collection;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;

public class arraylist {
	
	public static void main(String[] args) {
		
		//LinkedList list = new LinkedList();
		ArrayList list = new ArrayList();
		
		System.out.println("Before adding element:-"+list.size());
		list.add('a');
		list.add('b');
		list.add('c');
		list.add('d');
		list.add('d'); // duplicates allowed in arraylist
		list.add('e');
		list.add(null);  //null allowes
	
		
		System.out.println("after adding element:-"+list.size());
		
		System.out.println(list);
		
		list.remove(2);
		System.out.println("After removing object at 2nd index:-"+list);
		
		
		//Iterator
		Iterator<String> itr = list.iterator();
		
		while(itr.hasNext()) {
			
			Object itr_value = itr.next();
			System.out.print(itr_value);
			
		}
		
	}
	

}
