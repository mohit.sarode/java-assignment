package All_oops_Concept;

public class Two extends One {

	// v@Override

	public void display() {

		System.out.println("Two");

	}

	public int add(int x, int y) {

		return x + y;

	}

	// Overload

	public double add(double x, double y) {

		return x + y;

	}

}
