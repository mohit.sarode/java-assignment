package Inheritence;

public class childClass extends parentClass {

	public void property2() {
		System.out.println("Property 2");
	}
	
	public static void main(String[] args) {
		
		parentClass p = new parentClass();
		p.property1();
	//	p.property2();//can not access child class property in parent class
		
		childClass c = new childClass();
		c.property1();
		c.property2(); //can access both of the method parent and child
		
	} 
}
