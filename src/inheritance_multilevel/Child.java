package inheritance_multilevel;

public class Child extends Parent {
	public void property3() {
		System.out.println("Property 3");
	}
	
	public static void main(String[] args) {
		
		Child c = new Child();
		c.property1();            // Access all property in child class
		c.property2();
		c.property3();
		
		GrandParent g = new GrandParent();
		g.property1();          // access only grand parent property in Gradparent class
		//g.property2();
		//g.property3();
		
		Parent p = new Parent();
		
		p.property1();       // access  GrandParent property and Parent property in parent class
		p.property2();
	  //p.property3();
		
		
		
		
	}

}
